<%@page import="com.toystore.connection.Util"%>
<%@page import="java.io.FileOutputStream"%>
<%@page import="java.io.File"%>
<%@page import="java.io.DataInputStream"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<!DOCTYPE html>
<html lang="zxx">
    <head>
        <title>childhoodcomplete</title>
        <meta charset="UTF-8">
        <meta name="description" content=" Divisima | eCommerce Template">
        <meta name="keywords" content="divisima, eCommerce, creative, html">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Favicon -->
        <link href="img/logo.png" rel="shortcut icon"/>

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300,300i,400,400i,700,700i" rel="stylesheet">


        <!-- Stylesheets -->
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <link rel="stylesheet" href="css/font-awesome.min.css"/>
        <link rel="stylesheet" href="css/flaticon.css"/>
        <link rel="stylesheet" href="css/slicknav.min.css"/>
        <link rel="stylesheet" href="css/jquery-ui.min.css"/>
        <link rel="stylesheet" href="css/owl.carousel.min.css"/>
        <link rel="stylesheet" href="css/animate.css"/>
        <link rel="stylesheet" href="css/style.css"/>
        
        <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>

        <!--[if lt IE 9]>
                  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>
    <body>
        <%@ include file="header.jsp" %>
        <%
            if (session.getAttribute("email") == null /*|| !session.getAttribute("email").equals("admin")*/) {
                response.sendRedirect("home.jsp");
            }

            String productId = request.getParameter("productId");

            if (request.getParameter("type") != null && request.getParameter("type").equals("Delete")) {
                Statement st2 = db.connection.createStatement();
                String q2 = "delete from product where id=" + productId;
                st2.executeUpdate(q2);
                session.setAttribute("sM2", "Deleted Successfully");
                response.sendRedirect("result.jsp?allProducts=true");
            }

            String type = "Insert";
            if (request.getParameter("type") != null && request.getParameter("type").equals("Update")) {
                type = "Update";
            }

            Boolean isImageUpload = null;
            if (request.getParameter("isImageUpload") != null && request.getParameter("isImageUpload").equals("true")) {
                isImageUpload = true;
            }

            String name = request.getParameter("name") == null ? "" : request.getParameter("name");
            String details = request.getParameter("details") == null ? "" : request.getParameter("details");
            String tags = request.getParameter("tags") == null ? "" : request.getParameter("tags");
            String codes = request.getParameter("codes") == null ? "" : request.getParameter("codes");
            String playCategory = request.getParameter("playCategory") == null ? "" : request.getParameter("playCategory");
            String color = request.getParameter("color") == null ? "" : request.getParameter("color");
            String subCategoryId = request.getParameter("subCategoryId") == null ? "" : request.getParameter("subCategoryId");

            String toyLine1 = request.getParameter("toyLine1") == null ? "" : request.getParameter("toyLine1");
            String toyLine2 = request.getParameter("toyLine2") == null ? "" : request.getParameter("toyLine2");
            String toyLine3 = request.getParameter("toyLine3") == null ? "" : request.getParameter("toyLine3");
            String toyLine4 = request.getParameter("toyLine4") == null ? "" : request.getParameter("toyLine4");
            String toyLine5 = request.getParameter("toyLine5") == null ? "" : request.getParameter("toyLine5");
            String toyLine6 = request.getParameter("toyLine6") == null ? "" : request.getParameter("toyLine6");
            String toyLine7 = request.getParameter("toyLine7") == null ? "" : request.getParameter("toyLine7");
            String toyLine8 = request.getParameter("toyLine8") == null ? "" : request.getParameter("toyLine8");
            String toyLine9 = request.getParameter("toyLine9") == null ? "" : request.getParameter("toyLine9");
            String toyLine10 = request.getParameter("toyLine10") == null ? "" : request.getParameter("toyLine10");
            
            String partName1 = request.getParameter("partName1") == null ? "" : request.getParameter("partName1");
            String partName2 = request.getParameter("partName2") == null ? "" : request.getParameter("partName2");
            String partName3 = request.getParameter("partName3") == null ? "" : request.getParameter("partName3");
            String partName4 = request.getParameter("partName4") == null ? "" : request.getParameter("partName4");
            String partName5 = request.getParameter("partName5") == null ? "" : request.getParameter("partName5");
            String partName6 = request.getParameter("partName6") == null ? "" : request.getParameter("partName6");
            String partName7 = request.getParameter("partName7") == null ? "" : request.getParameter("partName7");
            String partName8 = request.getParameter("partName8") == null ? "" : request.getParameter("partName8");
            String partName9 = request.getParameter("partName9") == null ? "" : request.getParameter("partName9");
            String partName10 = request.getParameter("partName10") == null ? "" : request.getParameter("partName10");
            
            String brand1 = request.getParameter("brand1") == null ? "" : request.getParameter("brand1");
            String brand2 = request.getParameter("brand2") == null ? "" : request.getParameter("brand2");
            String brand3 = request.getParameter("brand3") == null ? "" : request.getParameter("brand3");
            String brand4 = request.getParameter("brand4") == null ? "" : request.getParameter("brand4");
            String brand5 = request.getParameter("brand5") == null ? "" : request.getParameter("brand5");
            String brand6 = request.getParameter("brand6") == null ? "" : request.getParameter("brand6");
            String brand7 = request.getParameter("brand7") == null ? "" : request.getParameter("brand7");
            String brand8 = request.getParameter("brand8") == null ? "" : request.getParameter("brand8");
            String brand9 = request.getParameter("brand9") == null ? "" : request.getParameter("brand9");
            String brand10 = request.getParameter("brand10") == null ? "" : request.getParameter("brand10");
            
            String associatedToy1 = request.getParameter("associatedToy1") == null ? "" : request.getParameter("associatedToy1");
            String associatedToy2 = request.getParameter("associatedToy2") == null ? "" : request.getParameter("associatedToy2");
            String associatedToy3 = request.getParameter("associatedToy3") == null ? "" : request.getParameter("associatedToy3");
            String associatedToy4 = request.getParameter("associatedToy4") == null ? "" : request.getParameter("associatedToy4");
            String associatedToy5 = request.getParameter("associatedToy5") == null ? "" : request.getParameter("associatedToy5");
            String associatedToy6 = request.getParameter("associatedToy6") == null ? "" : request.getParameter("associatedToy6");
            String associatedToy7 = request.getParameter("associatedToy7") == null ? "" : request.getParameter("associatedToy7");
            String associatedToy8 = request.getParameter("associatedToy8") == null ? "" : request.getParameter("associatedToy8");
            String associatedToy9 = request.getParameter("associatedToy9") == null ? "" : request.getParameter("associatedToy9");
            String associatedToy10 = request.getParameter("associatedToy10") == null ? "" : request.getParameter("associatedToy10");

            String year1 = request.getParameter("year1") == null ? "" : request.getParameter("year1");
            String year2 = request.getParameter("year2") == null ? "" : request.getParameter("year2");
            String year3 = request.getParameter("year3") == null ? "" : request.getParameter("year3");
            String year4 = request.getParameter("year4") == null ? "" : request.getParameter("year4");
            String year5 = request.getParameter("year5") == null ? "" : request.getParameter("year5");
            String year6 = request.getParameter("year6") == null ? "" : request.getParameter("year6");
            String year7 = request.getParameter("year7") == null ? "" : request.getParameter("year7");
            String year8 = request.getParameter("year8") == null ? "" : request.getParameter("year8");
            String year9 = request.getParameter("year9") == null ? "" : request.getParameter("year9");
            String year10 = request.getParameter("year10") == null ? "" : request.getParameter("year10");

            int additionalImages = 0;
            
            if (isImageUpload != null && isImageUpload) {
                //file start
                String saveFile = new String();
                String saveFile2 = new String();
                String contentType = request.getContentType();
                if ((contentType != null) && (contentType.indexOf("multipart/form-data") >= 0)) {
                    DataInputStream in = new DataInputStream(request.getInputStream());

                    int formDataLength = request.getContentLength();
                    byte dataBytes[] = new byte[formDataLength];
                    int byteRead = 0;
                    int totalBytesRead = 0;

                    while (totalBytesRead < formDataLength) {
                        byteRead = in.read(dataBytes, totalBytesRead, formDataLength);
                        totalBytesRead += byteRead;
                    }
                    String file = new String(dataBytes);

                    saveFile = file.substring(file.indexOf("filename=\"") + 10);
                    saveFile = saveFile.substring(0, saveFile.indexOf("\n"));
                    saveFile = saveFile.substring(saveFile.lastIndexOf("\\") + 1, saveFile.indexOf("\""));

                    int lastIndex = contentType.lastIndexOf("=");

                    String boundary = contentType.substring(lastIndex + 1, contentType.length());

                    int pos;

                    pos = file.indexOf("filename=\"");
                    pos = file.indexOf("\n", pos) + 1;
                    pos = file.indexOf("\n", pos) + 1;
                    pos = file.indexOf("\n", pos) + 1;

                    int boundaryLocation = file.indexOf(boundary, pos) - 4;

                    int startPos = ((file.substring(0, pos)).getBytes()).length;
                    int endPos = ((file.substring(0, boundaryLocation)).getBytes()).length;

                    String fileType = request.getParameter("fileType") != null ? request.getParameter("fileType").replace("additional", "_") : "";
                    
                    saveFile = "C:\\Users\\md_al\\Documents\\NetBeansProjects\\ToyStore\\build\\web\\img\\products\\" + productId + fileType + ".jpg";
                    saveFile2 = "C:\\Users\\md_al\\Documents\\NetBeansProjects\\ToyStore\\web\\img\\products\\" + productId + fileType + ".jpg";
                    //saveFile = "C:/uploadDir2/" + saveFile;
                    //out.print(saveFile);
                    File ff = new File(saveFile);
                    File ff2 = new File(saveFile2);

                    try {
                        FileOutputStream fileOut = new FileOutputStream(ff);
                        fileOut.write(dataBytes, startPos, (endPos - startPos));
                        fileOut.flush();
                        fileOut.close();
                        FileOutputStream fileOut2 = new FileOutputStream(ff2);
                        fileOut2.write(dataBytes, startPos, (endPos - startPos));
                        fileOut2.flush();
                        fileOut2.close();
                        
                        if(request.getParameter("fileType") != null) {
                        additionalImages = Integer.parseInt(fileType.replace("_",""));
                        }
                        
                        session.setAttribute("sM", "Uploaded Successfully");
                    } catch (Exception e) {
                        out.println(e);
                    }

                }
            }

            if (!color.equals("") && type.equals("Insert")) {

                Statement st2 = db.connection.createStatement();
                String q2 = "insert into product (id,name,details,codes,tags,color,play_category,sub_category_id,"
                        + "toy_lines,part_names,brands,associated_toys,years,additional_images) values("
                        + productId + ",'" + name + "','" + details + "','" + codes + "','"
                        + tags + "','" + color + "','" + playCategory + "'," + subCategoryId
                        + ",'" + String.join("#", toyLine1, toyLine2, toyLine3, toyLine4, toyLine5, toyLine6, toyLine7, toyLine8, toyLine9, toyLine10) + "','"
                        + String.join("#", partName1, partName2, partName3, partName4, partName5, partName6, partName7, partName8, partName9, partName10) + "','"
                        + String.join("#", brand1, brand2, brand3, brand4, brand5, brand6, brand7, brand8, brand9, brand10) + "','"
                        + String.join("#", associatedToy1, associatedToy2, associatedToy3, associatedToy4, associatedToy5, associatedToy6, associatedToy7, associatedToy8, associatedToy9, associatedToy10)
                        + "','" + String.join("#", year1, year2, year3, year4, year5, year6, year7, year8, year9, year10) + "'"
                        + "," +request.getParameter("additionalImages") + ")";
                st2.executeUpdate(q2);
                productId = null;
                session.setAttribute("sM", "Successfully Inserted!");
            } else if (!color.equals("")) {
                Statement st2 = db.connection.createStatement();
                String q2 = "update product set name='" + name + "', details='" + details + "', color='" + color + "', play_category='" + playCategory + "', codes='" + codes + "', tags='" + tags
                        + "', sub_category_id=" + subCategoryId
                        + ", toy_lines='" + String.join("#", toyLine1, toyLine2, toyLine3, toyLine4, toyLine5, toyLine6, toyLine7, toyLine8, toyLine9, toyLine10)
                        + "', part_names='" + String.join("#", partName1, partName2, partName3, partName4, partName5, partName6, partName7, partName8, partName9, partName10)
                        + "', brands='" + String.join("#", brand1, brand2, brand3, brand4, brand5, brand6, brand7, brand8, brand9, brand10)
                        + "', associated_toys='" + String.join("#", associatedToy1, associatedToy2, associatedToy3, associatedToy4, associatedToy5, associatedToy6, associatedToy7, associatedToy8, associatedToy9, associatedToy10)
                        + "', years='" + String.join("#", year1, year2, year3, year4, year5, year6, year7, year8, year9, year10)
                        + "', additional_images="+request.getParameter("additionalImages")
                        + " where id=" + productId;
                st2.executeUpdate(q2);
                session.setAttribute("sM", "Successfully Updated!");
            }

            if (productId == null) {
                Statement st = db.connection.createStatement();
                String q = "select max(id)+1 as mx from product";
                ResultSet rs = st.executeQuery(q);
                rs.next();
                productId = rs.getString("mx");
            } else {
                Statement st = db.connection.createStatement();
                String q = "select * from product where id=" + productId;
                ResultSet rs = st.executeQuery(q);
                if(rs.next()) {

                name = rs.getString("name");
                details = rs.getString("details");
                tags = rs.getString("tags");
                codes = rs.getString("codes");
                playCategory = rs.getString("play_category");
                color = rs.getString("color");
                subCategoryId = rs.getString("sub_category_id");

                String toyLines[] = Util.getSplittedTexts(rs.getString("toy_lines"));
                toyLine1 = toyLines[0];
                toyLine2 = toyLines[1];
                toyLine3 = toyLines[2];
                toyLine4 = toyLines[3];
                toyLine5 = toyLines[4];
                toyLine6 = toyLines[5];
                toyLine7 = toyLines[6];
                toyLine8 = toyLines[7];
                toyLine9 = toyLines[8];
                toyLine10 = toyLines[9];
                
                String partNames[] = Util.getSplittedTexts(rs.getString("part_names"));
                partName1 = partNames[0];
                partName2 = partNames[1];
                partName3 = partNames[2];
                partName4 = partNames[3];
                partName5 = partNames[4];
                partName6 = partNames[5];
                partName7 = partNames[6];
                partName8 = partNames[7];
                partName9 = partNames[8];
                partName10 = partNames[9];
                
                String brands[] = Util.getSplittedTexts(rs.getString("brands"));
                brand1 = brands[0];
                brand2 = brands[1];
                brand3 = brands[2];
                brand4 = brands[3];
                brand5 = brands[4];
                brand6 = brands[5];
                brand7 = brands[6];
                brand8 = brands[7];
                brand9 = brands[8];
                brand10 = brands[9];

                String associatedToys[] = Util.getSplittedTexts(rs.getString("associated_toys"));
                associatedToy1 = associatedToys[0];
                associatedToy2 = associatedToys[1];
                associatedToy3 = associatedToys[2];
                associatedToy4 = associatedToys[3];
                associatedToy5 = associatedToys[4];
                associatedToy6 = associatedToys[5];
                associatedToy7 = associatedToys[6];
                associatedToy8 = associatedToys[7];
                associatedToy9 = associatedToys[8];
                associatedToy10 = associatedToys[9];

                String years[] = Util.getSplittedTexts(rs.getString("years"));
                year1 = years[0];
                year2 = years[1];
                year3 = years[2];
                year4 = years[3];
                year5 = years[4];
                year6 = years[5];
                year7 = years[6];
                year8 = years[7];
                year9 = years[8];
                year10 = years[9];
                }

            }

        %>


        <!-- Page info -->
        <div class="page-top-info">
            <div class="container">
                <h4>
                    <%=type%>
                </h4>

                <div class="site-pagination">
                    <a href="home.jsp">Home</a> /
                    <a href="#"><%=type%></a>
                </div>
            </div>
        </div>
        <!-- Page info end -->

        <%if (session.getAttribute("sM") != null) {%>
        <div class="alert alert-success text-center">
            <%=session.getAttribute("sM")%>
        </div>
        <%session.setAttribute("sM", null);
            }%>
        <%if (session.getAttribute("eM") != null) {%>
        <div class="alert alert-danger text-center">
            <%=session.getAttribute("eM")%>
        </div>
        <%session.setAttribute("eM", null);
            }%>

        <!-- Contact section -->
        <section class="contact-section">
            <div class="container">
                <div class="row">
                    <img style="height: 100px;width: auto;" src="img/products/<%=productId%>.jpg">
                </div>
            </div>
            <%--<div class="map"><iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d14376.077865872314!2d-73.879277264103!3d40.757667781624285!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sbd!4v1546528920522" style="border:0" allowfullscreen></iframe></div>--%>
        </section>
        <!-- Contact section end -->

        <!-- Contact section -->
        <section class="contact-section">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 contact-info">
                        <form class="contact-form" enctype="multipart/form-data" 
                              action="addProduct.jsp?productId=<%=productId%>&type=<%=type%>&isImageUpload=true" method="post">
                            <input type="file" name="file" value="" required=""/>
                            <button class="site-btn" type="submit">Upload Image</button>
                        </form>
                    </div>
                </div>
            </div>
            <%--<div class="map"><iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d14376.077865872314!2d-73.879277264103!3d40.757667781624285!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sbd!4v1546528920522" style="border:0" allowfullscreen></iframe></div>--%>
        </section>
        <section class="contact-section">
            <div class="container">
                <br>
                <div class="row">
                    <div class="col-lg-6 contact-info">
                        <form class="contact-form" enctype="multipart/form-data" 
                              action="addProduct.jsp?productId=<%=productId%>&type=<%=type%>&isImageUpload=true&fileType=additional1" method="post">
                            <input type="file" name="file1" value="" required=""/>
                            <button class="site-btn" type="submit">Upload Additional Image 1</button>
                        </form>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-lg-6 contact-info">
                        <form class="contact-form" enctype="multipart/form-data" 
                              action="addProduct.jsp?productId=<%=productId%>&type=<%=type%>&isImageUpload=true&fileType=additional2" method="post">
                            <input type="file" name="file2" value="" required=""/>
                            <button class="site-btn" type="submit">Upload Additional Image 2</button>
                        </form>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-lg-6 contact-info">
                        <form class="contact-form" enctype="multipart/form-data" 
                              action="addProduct.jsp?productId=<%=productId%>&type=<%=type%>&isImageUpload=true&fileType=additional3" method="post">
                            <input type="file" name="file3" value="" required=""/>
                            <button class="site-btn" type="submit">Upload Additional Image 3</button>
                        </form>
                    </div>
                </div>
            </div><br>
            <%--<div class="map"><iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d14376.077865872314!2d-73.879277264103!3d40.757667781624285!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sbd!4v1546528920522" style="border:0" allowfullscreen></iframe></div>--%>
        </section>
        <!-- Contact section end -->

        <!-- Contact section -->
        <section class="contact-section">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 contact-info">
                        <form class="contact-form" action="addProduct.jsp" method="post">
                            <%--PART NAME<input type="text" name="name" value="<%=name%>" placeholder="PART NAME">--%>

                            PART NAME #1<input type="text" name="partName1" value="<%=partName1%>" placeholder="PART NAME #1">
                            PART NAME #2<input type="text" name="partName2" value="<%=partName2%>" placeholder="PART NAME #2">
                            PART NAME #3<input type="text" name="partName3" value="<%=partName3%>" placeholder="PART NAME #3">
                            PART NAME #4<input type="text" name="partName4" value="<%=partName4%>" placeholder="PART NAME #4">
                            PART NAME #5<input type="text" name="partName5" value="<%=partName5%>" placeholder="PART NAME #5">
                            PART NAME #6<input type="text" name="partName6" value="<%=partName6%>" placeholder="PART NAME #6">
                            PART NAME #7<input type="text" name="partName7" value="<%=partName7%>" placeholder="PART NAME #7">
                            PART NAME #8<input type="text" name="partName8" value="<%=partName8%>" placeholder="PART NAME #8">
                            PART NAME #9<input type="text" name="partName9" value="<%=partName9%>" placeholder="PART NAME #9">
                            PART NAME #10<input type="text" name="partName10" value="<%=partName10%>" placeholder="PART NAME #10">

                            TOY LINE #1<input type="text" name="toyLine1" value="<%=toyLine1%>" placeholder="TOY LINE #1">
                            TOY LINE #2<input type="text" name="toyLine2" value="<%=toyLine2%>" placeholder="TOY LINE #2">
                            TOY LINE #3<input type="text" name="toyLine3" value="<%=toyLine3%>" placeholder="TOY LINE #3">
                            TOY LINE #4<input type="text" name="toyLine4" value="<%=toyLine4%>" placeholder="TOY LINE #4">
                            TOY LINE #5<input type="text" name="toyLine5" value="<%=toyLine5%>" placeholder="TOY LINE #5">
                            TOY LINE #6<input type="text" name="toyLine6" value="<%=toyLine6%>" placeholder="TOY LINE #6">
                            TOY LINE #7<input type="text" name="toyLine7" value="<%=toyLine7%>" placeholder="TOY LINE #7">
                            TOY LINE #8<input type="text" name="toyLine8" value="<%=toyLine8%>" placeholder="TOY LINE #8">
                            TOY LINE #9<input type="text" name="toyLine9" value="<%=toyLine9%>" placeholder="TOY LINE #9">
                            TOY LINE #10<input type="text" name="toyLine10" value="<%=toyLine10%>" placeholder="TOY LINE #10">
                            
                            ASSOCIATED TOY 1 NAME<input type="text" name="associatedToy1" value="<%=associatedToy1%>" placeholder="ASSOCIATED TOY 1 NAME">
                            ASSOCIATED TOY 2 NAME<input type="text" name="associatedToy2" value="<%=associatedToy2%>" placeholder="ASSOCIATED TOY 2 NAME">
                            ASSOCIATED TOY 3 NAME<input type="text" name="associatedToy3" value="<%=associatedToy3%>" placeholder="ASSOCIATED TOY 3 NAME">
                            ASSOCIATED TOY 4 NAME<input type="text" name="associatedToy4" value="<%=associatedToy4%>" placeholder="ASSOCIATED TOY 4 NAME">
                            ASSOCIATED TOY 5 NAME<input type="text" name="associatedToy5" value="<%=associatedToy5%>" placeholder="ASSOCIATED TOY 5 NAME">
                            ASSOCIATED TOY 6 NAME<input type="text" name="associatedToy6" value="<%=associatedToy6%>" placeholder="ASSOCIATED TOY 6 NAME">
                            ASSOCIATED TOY 7 NAME<input type="text" name="associatedToy7" value="<%=associatedToy7%>" placeholder="ASSOCIATED TOY 7 NAME">
                            ASSOCIATED TOY 8 NAME<input type="text" name="associatedToy8" value="<%=associatedToy8%>" placeholder="ASSOCIATED TOY 8 NAME">
                            ASSOCIATED TOY 9 NAME<input type="text" name="associatedToy9" value="<%=associatedToy9%>" placeholder="ASSOCIATED TOY 9 NAME">
                            ASSOCIATED TOY 10 NAME<input type="text" name="associatedToy10" value="<%=associatedToy10%>" placeholder="ASSOCIATED TOY 10 NAME">

                            YEAR #1<input type="text" name="year1" value="<%=year1%>" placeholder="YEAR #1">
                            YEAR #2<input type="text" name="year2" value="<%=year2%>" placeholder="YEAR #2">
                            YEAR #3<input type="text" name="year3" value="<%=year3%>" placeholder="YEAR #3">
                            YEAR #4<input type="text" name="year4" value="<%=year4%>" placeholder="YEAR #4">
                            YEAR #5<input type="text" name="year5" value="<%=year5%>" placeholder="YEAR #5">
                            YEAR #6<input type="text" name="year6" value="<%=year6%>" placeholder="YEAR #6">
                            YEAR #7<input type="text" name="year7" value="<%=year7%>" placeholder="YEAR #7">
                            YEAR #8<input type="text" name="year8" value="<%=year8%>" placeholder="YEAR #8">
                            YEAR #9<input type="text" name="year9" value="<%=year9%>" placeholder="YEAR #9">
                            YEAR #10<input type="text" name="year10" value="<%=year10%>" placeholder="YEAR #10">
                            
                            BRAND #1<input type="text" name="brand1" value="<%=brand1%>" placeholder="BRAND #1">
                            BRAND #2<input type="text" name="brand2" value="<%=brand2%>" placeholder="BRAND #2">
                            BRAND #3<input type="text" name="brand3" value="<%=brand3%>" placeholder="BRAND #3">
                            BRAND #4<input type="text" name="brand4" value="<%=brand4%>" placeholder="BRAND #4">
                            BRAND #5<input type="text" name="brand5" value="<%=brand5%>" placeholder="BRAND #5">
                            BRAND #6<input type="text" name="brand6" value="<%=brand6%>" placeholder="BRAND #6">
                            BRAND #7<input type="text" name="brand7" value="<%=brand7%>" placeholder="BRAND #7">
                            BRAND #8<input type="text" name="brand8" value="<%=brand8%>" placeholder="BRAND #8">
                            BRAND #9<input type="text" name="brand9" value="<%=brand9%>" placeholder="BRAND #9">
                            BRAND #10<input type="text" name="brand10" value="<%=brand10%>" placeholder="BRAND #10">

                            Additional details<textarea name="details" placeholder="Additional details"><%=details%></textarea>

                            Tags (separated by space)<textarea name="tags" placeholder="tags (separated by space)"><%=tags%></textarea>
                            <%--<textarea name="codes" placeholder="codes (separated by space)" required=""><%=codes%></textarea>--%>

                            <select name="color" class="form-control" required="">
                                <option value="">--Select Color--</option>
                                <%
                                    Statement stColor = db.connection.createStatement();
                                    String qColor = "select * from sub_category where category_id=1";
                                    ResultSet rsColor = stColor.executeQuery(qColor);

                                    while (rsColor.next()) {
                                %>
                                <option value="<%=rsColor.getString("name")%>" <%if (rsColor.getString("name").equals(color)) {%>selected<%}%> ><%=rsColor.getString("name")%></option>
                                <%}%>
                            </select>
                            <br>
                            <select name="playCategory" class="form-control" required="" onchange="setSubCategory(this.value);">
                                <option value="">--Select Play Category--</option>
                                <%
                                    Statement stPlayCategory = db.connection.createStatement();
                                    String qPlayCategory = "select * from sub_category where category_id=2";
                                    ResultSet rsPlayCategory = stPlayCategory.executeQuery(qPlayCategory);

                                    while (rsPlayCategory.next()) {
                                %>
                                <option value="<%=rsPlayCategory.getString("name")%>" <%if (rsPlayCategory.getString("name").equals(playCategory)) {%>selected<%}%> ><%=rsPlayCategory.getString("name")%></option>
                                <%}%>
                            </select>
                            <br>
                            <select name="subCategoryId" class="form-control" required="true">
                                <option value="">--Select Sub Category--</option>
                                <%
                                    Statement st2 = db.connection.createStatement();
                                    String q2 = "select * from category where id>2";
                                    ResultSet rs2 = st2.executeQuery(q2);
                                    while (rs2.next()) {
                                        Statement st3 = db.connection.createStatement();
                                        String q3 = "select * from sub_category where category_id=" + rs2.getString("id");
                                        ResultSet rs3 = st3.executeQuery(q3);
                                        while (rs3.next()) {
                                %>
                                <option class="allsub <%=rs2.getString("id")%>" value="<%=rs3.getString("id")%>" <%if (rs3.getString("id").equals(subCategoryId)) {%>selected<%}%>><%=rs2.getString("name")%> -> <%=rs3.getString("name")%></option>
                                <%}
                                    }%>
                            </select>
                            <input type="hidden" name="type" value="<%=type%>">
                            <input type="hidden" name="productId" value="<%=productId%>">
                            <input type="hidden" name="additionalImages" value="<%=additionalImages%>">
                            <br>
                            <button class="site-btn" type="submit"><%=type%></button>
                        </form>
                    </div>
                </div>
            </div>
            <%--<div class="map"><iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d14376.077865872314!2d-73.879277264103!3d40.757667781624285!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sbd!4v1546528920522" style="border:0" allowfullscreen></iframe></div>--%>
        </section>
        <!-- Contact section end -->



        <br><br><br><br><br><br><br><br><br><br><br>


        <%@ include file="footer.jsp" %>
        <script>
            function setSubCategory(val) {

                var ar = [];
                ar[3] = 'ACTION FIGURE';
                ar[4] = 'VEHICLE';
                ar[5] = 'PLAYSET';
                ar[6] = 'ROLEPLAY';
                ar[7] = 'GAME / BOARDGAME';
                ar[8] = 'FOOD PREMIUM';


                for (var i = 3; i <= 8; i++) {
                    var x = document.getElementsByClassName(i);
                    //alert(val+"-"+i);
                    for (var j = 0; j < x.length; j++) {
                        if (val != ar[i]) {
                            x[j].style.display = 'none';
                            //alert("yeeee");
                        } else {
                            x[j].style.display = 'block';
                            //alert("zeeee");
                        }
                    }
                }
            }

        </script>

    </body>
</html>
