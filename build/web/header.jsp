<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="com.toystore.connection.Database"%>
<%
Database db = new Database();
        db.connect();
       //try{
           
        Statement stC = db.connection.createStatement();
        String qC = "select * from category";
        ResultSet rsC = stC.executeQuery(qC);
%>
<style>
    .row {
    margin-right: 0px;
    margin-left: 0px;
}
</style>
<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>

<!-- Header section -->
	<header class="header-section">
		<div class="header-top">
			<div class="container">
				<div class="row">
					<div class="col-lg-2 text-center text-lg-left">
						<!-- logo -->
						<a href="home.jsp" class="site-logo">
                                                    <img src="img/logo.png" alt="" style="height: 70px;width:auto;">
						</a>
					</div>
					<div class="col-xl-6 col-lg-5">
                                            <form class="header-search-form" action="result.jsp">
							<input type="text" name="searchName" placeholder="Search for a Part">
							<button><i class="flaticon-search"></i></button>
						</form>
					</div>
					<div class="col-xl-4 col-lg-5">
						<div class="user-panel">
							<div class="up-item">
								<i class="flaticon-profile"></i>
                                                                <%if(session.getAttribute("id")==null){%>
                                                                <a href="login.jsp?type=Login">Sign</a> In 
                                                                <%}else{%>
                                                                Logged in as <b><%=session.getAttribute("email")%></b>
                                                                <br>
                                                                <a href="login.jsp?Logout=true"><b>logout</b></a>
                                                                <%if(session.getAttribute("email")!=null&&session.getAttribute("email").equals("admin")){%>&nbsp;or&nbsp;<a href="login.jsp?type=Register"><b>Create Account</b></a><%}%>
                                                                <%}%>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<nav class="main-navbar">
			<div class="container">
				<!-- menu -->
				<ul class="main-menu">
					<li><a href="home.jsp">HOME</a></li>
                                        <%if(session.getAttribute("email")!=null /*&&session.getAttribute("email").equals("admin")*/){%>
                                        <li><a href="addProduct.jsp">ADD PRODUCT</a></li>
                                        <li><a href="addCategory.jsp">ADD CATEGORY</a></li>
                                        <li><a href="result.jsp?allProducts=true">ALL PARTS</a></li>
                                        <li><a href="changeBanner.jsp">CHANGE BANNER</a></li>
                                        <li><a href="changeDetails.jsp">CHANGE DETAILS</a></li>
                                        <li><a href="addBlog.jsp">ADD BLOG POST</a></li>
                                        <li><a href="bulkUpload.jsp">BULK IMAGE UPLOAD</a></li>
                                        <%}%>
                                        
                                        <%--
                                        <%
                                                        while(rsC.next()) {
                                        %>
					<li><a href="#"><%=rsC.getString("name")%></a>
                                        <%
                                                        Statement stS = db.connection.createStatement();
                                                        String qS = "select * from sub_category where category_id="+rsC.getString("id");
                                                        ResultSet rsS = stS.executeQuery(qS); 
                                                        while(rsS.next()) {
                                                            
                                                        if(rsS.isFirst()){
                                                        %>
                                                        <ul class="sub-menu"><%}%>
							<li><a href="home.jsp?categoryId=<%=rsC.getString("id")%>&subCategoryId=<%=rsS.getString("id")%>"><%=rsS.getString("name")%></a></li>
						<%if(rsS.isLast()){%></ul><%}%>
                                        <%}%>
                                        </li>
                                        <%}%>
                                        --%>
				</ul>
			</div>
		</nav>
	</header>
	<!-- Header section end -->