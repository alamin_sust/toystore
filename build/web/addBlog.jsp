<%-- 
    Document   : admin
    Created on : Apr 10, 2019, 4:36:26 AM
    Author     : md_al
--%>

<%@page import="java.io.FileOutputStream"%>
<%@page import="java.io.File"%>
<%@page import="java.io.DataInputStream"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<!DOCTYPE html>
<html lang="zxx">
    <head>
        <title>childhoodcomplete</title>
        <meta charset="UTF-8">
        <meta name="description" content=" Divisima | eCommerce Template">
        <meta name="keywords" content="divisima, eCommerce, creative, html">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Favicon -->
        <link href="img/logo.png" rel="shortcut icon"/>

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300,300i,400,400i,700,700i" rel="stylesheet">


        <!-- Stylesheets -->
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <link rel="stylesheet" href="css/font-awesome.min.css"/>
        <link rel="stylesheet" href="css/flaticon.css"/>
        <link rel="stylesheet" href="css/slicknav.min.css"/>
        <link rel="stylesheet" href="css/jquery-ui.min.css"/>
        <link rel="stylesheet" href="css/owl.carousel.min.css"/>
        <link rel="stylesheet" href="css/animate.css"/>
        <link rel="stylesheet" href="css/style.css"/>

        <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>

        <!--[if lt IE 9]>
                  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>
    <body>
        <%@ include file="header.jsp" %>
        <%
            if (session.getAttribute("email") == null || !session.getAttribute("email").equals("admin")) {
                response.sendRedirect("home.jsp");
            }
            
            String titleDB = "";
            String detailsDB = "";
            String updateId = "";
            
            if (request.getParameter("deleteId") != null && !request.getParameter("deleteId").trim().equals("")) {
                Statement st1 = db.connection.createStatement();
                String q1 = "delete from blog where id = "+request.getParameter("deleteId");
                st1.executeUpdate(q1);
                session.setAttribute("sM", "Successfully Deleted!");
                response.sendRedirect("home.jsp");
            }
            
            if (request.getParameter("updateId") != null && !request.getParameter("updateId").trim().equals("")) {
                updateId = request.getParameter("updateId");
                Statement st1 = db.connection.createStatement();
                String q1 = "select * from blog where id = "+updateId;
                ResultSet rs1 = st1.executeQuery(q1);
                rs1.next();
                titleDB  = rs1.getString("title");
                detailsDB  = rs1.getString("details");
            }
            
            Boolean isImageUpload = null;
            if (request.getParameter("isImageUpload") != null && request.getParameter("isImageUpload").equals("true")) {
                isImageUpload = true;
            }
            
            Statement st1 = db.connection.createStatement();
                String q1 = "select max(id)+1 as nextId from blog";
                ResultSet rs1 = st1.executeQuery(q1);
                rs1.next();
                String nextId = rs1.getString("nextId");

            String title = request.getParameter("title") == null ? "" : request.getParameter("title");
            String details = request.getParameter("details") == null ? "" : request.getParameter("details");
            
            if (isImageUpload != null && isImageUpload) {
                //file start
                String saveFile = new String();
                String saveFile2 = new String();
                String contentType = request.getContentType();
                if ((contentType != null) && (contentType.indexOf("multipart/form-data") >= 0)) {
                    DataInputStream in = new DataInputStream(request.getInputStream());

                    int formDataLength = request.getContentLength();
                    byte dataBytes[] = new byte[formDataLength];
                    int byteRead = 0;
                    int totalBytesRead = 0;

                    while (totalBytesRead < formDataLength) {
                        byteRead = in.read(dataBytes, totalBytesRead, formDataLength);
                        totalBytesRead += byteRead;
                    }
                    String file = new String(dataBytes);

                    saveFile = file.substring(file.indexOf("filename=\"") + 10);
                    saveFile = saveFile.substring(0, saveFile.indexOf("\n"));
                    saveFile = saveFile.substring(saveFile.lastIndexOf("\\") + 1, saveFile.indexOf("\""));

                    int lastIndex = contentType.lastIndexOf("=");

                    String boundary = contentType.substring(lastIndex + 1, contentType.length());

                    int pos;

                    pos = file.indexOf("filename=\"");
                    pos = file.indexOf("\n", pos) + 1;
                    pos = file.indexOf("\n", pos) + 1;
                    pos = file.indexOf("\n", pos) + 1;

                    int boundaryLocation = file.indexOf(boundary, pos) - 4;

                    int startPos = ((file.substring(0, pos)).getBytes()).length;
                    int endPos = ((file.substring(0, boundaryLocation)).getBytes()).length;

                    saveFile = "C:\\Users\\md_al\\Documents\\NetBeansProjects\\ToyStore\\build\\web\\img\\blog\\"+(updateId.equals("")?nextId:updateId)+".jpg";
                    saveFile2 = "C:\\Users\\md_al\\Documents\\NetBeansProjects\\ToyStore\\web\\img\\blog\\"+(updateId.equals("")?nextId:updateId)+".jpg";
                    //saveFile = "C:/uploadDir2/" + saveFile;
                    //out.print(saveFile);
                    File ff = new File(saveFile);
                    File ff2 = new File(saveFile2);

                    try {
                        FileOutputStream fileOut = new FileOutputStream(ff);
                        fileOut.write(dataBytes, startPos, (endPos - startPos));
                        fileOut.flush();
                        fileOut.close();
                        FileOutputStream fileOut2 = new FileOutputStream(ff2);
                        fileOut2.write(dataBytes, startPos, (endPos - startPos));
                        fileOut2.flush();
                        fileOut2.close();
                        session.setAttribute("sM", "Uploaded Successfully");
                    } catch (Exception e) {
                        out.println(e);
                    }

                }
            }

            if (!title.equals("")) {
                if(updateId.equals("")) {
                Statement st2 = db.connection.createStatement();
                String q2 = "insert into blog (id,title,details) values("+nextId+",'"+title+"','"+details+"')";
                st2.executeUpdate(q2);
                session.setAttribute("sM", "Successfully Inserted!");
                } else {
                    Statement st2 = db.connection.createStatement();
                String q2 = "update blog set title='"+title+"',details='"+details+"' where id="+updateId;
                st2.executeUpdate(q2);
                session.setAttribute("sM", "Successfully Updated!");
                }
            }

        %>


        <!-- Page info -->
        <div class="page-top-info">
            <div class="container">
                <h4>
                    Admin Panel
                </h4>

                <div class="site-pagination">
                    <a href="#">Add Blog</a>
                </div>
            </div>
        </div>
        <!-- Page info end -->

        <%if (session.getAttribute("sM") != null) {%>
        <div class="alert alert-success text-center">
            <%=session.getAttribute("sM")%>
        </div>
        <%session.setAttribute("sM", null);
            }%>
        <%if (session.getAttribute("eM") != null) {%>
        <div class="alert alert-danger text-center">
            <%=session.getAttribute("eM")%>
        </div>
        <%session.setAttribute("eM", null);
            }%>

            
        <!-- Contact section -->
        <section class="contact-section">
            <div class="container">
                <div class="row">
                    <h2>Blog Post</h2>
                </div>
                <div class="row">
                    <div class="col-lg-6 contact-info">
                        <form class="contact-form" enctype="multipart/form-data" 
                              action="addBlog.jsp?isImageUpload=true&updateId=<%=updateId%>" method="post">
                            <input type="file" name="file" value="" required=""/>
                            <button class="site-btn" type="submit">Upload Image</button>
                        </form>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-lg-6 contact-info">
                        <form class="contact-form" action="addBlog.jsp" method="post">
                            <input type="text" name="title" value="<%=titleDB%>" placeholder="Title" required="">
                            <input type="text" name="details" value="<%=detailsDB%>" placeholder="Details" required="">
                            <br>
                            <input type="hidden" name="updateId" value="<%=updateId%>"/>
                            <button class="site-btn" type="submit">Update</button>
                        </form>
                    </div>
                </div>
            </div>
            <%--<div class="map"><iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d14376.077865872314!2d-73.879277264103!3d40.757667781624285!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sbd!4v1546528920522" style="border:0" allowfullscreen></iframe></div>--%>
        </section>
        <!-- Contact section end -->



        <br><br><br><br><br><br><br><br><br><br><br>


        <%@ include file="footer.jsp" %>


    </body>
</html>

