<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<!DOCTYPE html>
<html lang="zxx">
<head>
	<title>childhoodcomplete</title>
	<meta charset="UTF-8">
	<meta name="description" content=" Divisima | eCommerce Template">
	<meta name="keywords" content="divisima, eCommerce, creative, html">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Favicon -->
	<link href="img/logo.png" rel="shortcut icon"/>

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300,300i,400,400i,700,700i" rel="stylesheet">


	<!-- Stylesheets -->
	<link rel="stylesheet" href="css/bootstrap.min.css"/>
	<link rel="stylesheet" href="css/font-awesome.min.css"/>
	<link rel="stylesheet" href="css/flaticon.css"/>
	<link rel="stylesheet" href="css/slicknav.min.css"/>
	<link rel="stylesheet" href="css/jquery-ui.min.css"/>
	<link rel="stylesheet" href="css/owl.carousel.min.css"/>
	<link rel="stylesheet" href="css/animate.css"/>
	<link rel="stylesheet" href="css/style.css"/>


        <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
        
	<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>
<body>
    <%@ include file="header.jsp" %>
    <%
        if (session.getAttribute("email") == null /*|| !session.getAttribute("email").equals("admin")*/) {
                response.sendRedirect("home.jsp");
            }
        
        
        String catId = request.getParameter("catId")==null?"":request.getParameter("catId");
        String subCatId = request.getParameter("subCatId")==null?"":request.getParameter("subCatId");
        String name = request.getParameter("name")==null?"":request.getParameter("name");
        
        Statement st = db.connection.createStatement(); 
        String q = "select * from sub_category where id="+subCatId;
        ResultSet rs = st.executeQuery(q);
        rs.next();
        
        if(!name.equals("")) {
        
            Statement st1 = db.connection.createStatement();
        String q1 = "update sub_category set name='"+name+"' where id="+subCatId;
        st1.executeUpdate(q1);
            
            if(catId.equals("2")) {
            Statement st2 = db.connection.createStatement();
        String q2 = "update category set name='"+name+"' where name='"+rs.getString("name")+"'";
        st2.executeUpdate(q2);
        
                Statement st3 = db.connection.createStatement();
        String q3 = "update sub_category set category_name='"+name+"' where category_name='"+rs.getString("name")+"'";
        st3.executeUpdate(q3);
            }
            
        session.setAttribute("sM", "Successfully Updated!");
        } else if(request.getParameter("delete")!=null) {
            Statement st1 = db.connection.createStatement();
        String q1 = "delete from sub_category where id="+subCatId;
        st1.executeUpdate(q1);
        
        if(catId.equals("2")) {
            Statement st2 = db.connection.createStatement();
        String q2 = "delete from category where name='"+rs.getString("name")+"'";
        st2.executeUpdate(q2);
        
                Statement st3 = db.connection.createStatement();
        String q3 = "delete from sub_category where category_name='"+rs.getString("name")+"'";
        st3.executeUpdate(q3);
            }
        session.setAttribute("sM", "Successfully Deleted!");
        }
        
        
        
        
    %>
    

	<!-- Page info -->
	<div class="page-top-info">
		<div class="container">
                            
			<div class="site-pagination">
				<a href="#">Edit Category</a>
			</div>
		</div>
	</div>
	<!-- Page info end -->
        
        <%if(session.getAttribute("sM")!=null){%>
            <div class="alert alert-success text-center">
                <%=session.getAttribute("sM")%>
            </div>
        <%session.setAttribute("sM", null);}%>
        <%if(session.getAttribute("eM")!=null){%>
            <div class="alert alert-danger text-center">
                <%=session.getAttribute("eM")%>
            </div>
        <%session.setAttribute("eM", null);}%>
        
        <!-- Contact section -->
	<section class="contact-section">
		<div class="container">
                    <h2>Edit Categories</h2>
			<div class="row">
				<div class="col-lg-6 contact-info">
                                    <form class="contact-form" action="editCategory.jsp" method="post">
                                        <input type="text" name="name" value="<%=rs.getString("name")%>" placeholder="name" required="">
                                        <input type="hidden" name="catId" value="<%=catId%>"/>
                                        <input type="hidden" name="subCatId" value="<%=subCatId%>"/>
                                        <br>
                                            <button class="site-btn" type="submit">Edit</button>
					</form>
				</div>
			</div>
                                        <br>
                                        <h2>Delete Categories</h2>
			<div class="row">
				<div class="col-lg-6 contact-info">
                                    <form class="contact-form" action="editCategory.jsp" method="post">
                                        <input type="hidden" name="delete" value="true"/>
                                        <input type="hidden" name="catId" value="<%=catId%>"/>
                                        <input type="hidden" name="subCatId" value="<%=subCatId%>"/>
                                        <br>
                                            <button class="site-btn" type="submit">Delete</button>
					</form>
				</div>
			</div>
                </div>
		<%--<div class="map"><iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d14376.077865872314!2d-73.879277264103!3d40.757667781624285!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sbd!4v1546528920522" style="border:0" allowfullscreen></iframe></div>--%>
	</section>
	<!-- Contact section end -->

        <br><br><br><br><br><br><br><br><br><br><br>


        <%@ include file="footer.jsp" %>
	</body>
</html>
