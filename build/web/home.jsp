<!DOCTYPE html>
<html lang="zxx">
    <head>
        <title>childhoodcomplete</title>
        <meta charset="UTF-8">
        <meta name="description" content=" childhoodcomplete">
        <meta name="keywords" content="childhoodcomplete html">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Favicon -->
        <link href="img/logo.png" rel="shortcut icon"/>

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300,300i,400,400i,700,700i" rel="stylesheet">


        <!-- Stylesheets -->
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <link rel="stylesheet" href="css/font-awesome.min.css"/>
        <link rel="stylesheet" href="css/flaticon.css"/>
        <link rel="stylesheet" href="css/slicknav.min.css"/>
        <link rel="stylesheet" href="css/jquery-ui.min.css"/>
        <link rel="stylesheet" href="css/owl.carousel.min.css"/>
        <link rel="stylesheet" href="css/animate.css"/>
        <link rel="stylesheet" href="css/style.css"/>

        <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>


        <!--[if lt IE 9]>
                  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>
    <body>
        <script>
            function showHiddenText() {
                $("#complete").show();
                $("#more").hide();
            }
        </script>
        <style>
            
            .more{
                background:lightblue;
                color:navy;
                font-size:13px;
                padding:3px;
                cursor:pointer;
            }
        </style>
        <%@ include file="header.jsp" %>

        <%
            Statement stB = db.connection.createStatement();
            String qB = "select * from banner where id=1";
            ResultSet bannerRs = stB.executeQuery(qB);
            bannerRs.next();

            Statement stDet = db.connection.createStatement();
            String qDet = "select * from website_details where id=1";
            ResultSet rsDet = stDet.executeQuery(qDet);
            rsDet.next();

            String playCategory = request.getParameter("playCategory") == null ? "" : request.getParameter("playCategory");
            String color = request.getParameter("color") == null ? "" : request.getParameter("color");
            String subCategoryId = request.getParameter("subCategoryId") == null ? "" : request.getParameter("subCategoryId");
            String label = request.getParameter("label") == null ? "" : request.getParameter("label");

            Statement st = db.connection.createStatement();
            String q = "";
            String currStage = "";
            String currStageParam = "";
            String editCat="";
            String editSubCat="";

            if (color.equals("")) {
                q = "select id dbId, name id,name name from sub_category where category_id=1";
                currStage = "Color";
                currStageParam = "color";
                editCat="1";
            } else if (playCategory.equals("")) {
                q = "select id dbId, name id, name name from sub_category where category_id=2";
                currStage = "Play Category";
                currStageParam = "playCategory";
                label += " -> Color:" + color;
                editCat="2";
            } else if (subCategoryId.equals("")) {
                q = "select id dbId, id id, name name from sub_category where category_name='" + playCategory + "'";
                currStage = "Sub Category";
                currStageParam = "subCategoryId";
                label += " -> Play Category: " + playCategory;
            } else {
                q = "select sc.id dbId, sc.id,concat(c.name,' -> ',sc.name) name from sub_category sc join category c on(sc.category_id=c.id) where sc.id=" + subCategoryId;
                ResultSet rs = st.executeQuery(q);
                rs.next();
                label += " -> Sub Category: " + rs.getString("name");
                response.sendRedirect("result.jsp?color=" + color + "&playCategory=" + playCategory
                        + "&subCategoryId=" + subCategoryId + "&label=" + label);
            }
            String addParam = "?label=" + label + (color.equals("") ? "" : "&color=" + color)
                    + (playCategory.equals("") ? "" : "&playCategory=" + playCategory)
                    + (subCategoryId.equals("") ? "" : "&subCategoryId=" + subCategoryId);
            q+=" order by name";
            ResultSet rs = st.executeQuery(q);

        %>

        <!-- Hero section -->
        <div class="container" style="margin-bottom: 40px; padding: 0px;">
            <img src="img/banner.jpg" onclick="window.open('<%=bannerRs.getString("url")%>')" style="cursor: pointer;width: 100%; height: auto;"/>
        </div>

        <!-- Hero section end -->

        <!-- Page info -->
        <div class="page-top-info">
            <div class="container">
                <div class="site-pagination">
                    <%if (label != null && !label.equals("")) {%>
                    <a href=""><%=label%></a>
                    <%}%>
                </div>
            </div>
        </div>
        <!-- Page info end -->

        <%if (session.getAttribute("sM2") != null) {%>
        <div class="alert alert-success text-center">
            <%=session.getAttribute("sM2")%>
        </div>
        <%session.setAttribute("sM2", null);
            }%>



        <section class="contact-section">
            <div class="container" style="padding: 0px;">
                <div class="row">
                    <div class="col-lg-4 contact-info">
                        <h2 style="font-size: 1.7rem; color: red; font-family: 'Calibri'">Select <%=currStage%></h2>
                        
                            <%

                                while (rs.next()) {
                            %>
                            <span style="font-weight: bold; font-family: 'Calibri'">
                                <a style="color: black;" href="home.jsp<%=addParam%>&<%=currStageParam%>=<%=rs.getString("id")%>"><%=rs.getString("name")%></a>
                                <%if (session.getAttribute("email") != null && session.getAttribute("email").equals("admin")) {%>
                                <a href="editCategory.jsp?catId=<%=editCat%>&subCatId=<%=rs.getString("dbId")%>">edit</a>
                                <%}%>
                            </span>
                            <br>
                                <%}%>
                    </div>

                    <div class="col-lg-7 contact-info"  >
                        <div class="well" style="padding-top:30px;">
                            <%if (rsDet.getString("is_bold").equals("1")) {%>
                            <%if (rsDet.getString("font_size") != null) {%>
                            <h3 style="text-align:center; margin-bottom: 0px; font-weight: bold; font-size: <%=rsDet.getString("font_size")%>px;"><%=rsDet.getString("title")%></h3><br>
                            <p style="text-align:center; font-weight: bold; font-size: <%=rsDet.getString("font_size")%>px;"><%=rsDet.getString("details")%></p><br>
                            <%} else {%>
                            <h3 style="text-align:center; margin-bottom: 0px; font-weight: bold;"><%=rsDet.getString("title")%></h3><br>
                            <p style="text-align:center; font-weight: bold;"><%=rsDet.getString("details")%></p><br>
                            <%}%>
                            <%} else {%>
                            <%if (rsDet.getString("font_size") != null) {%>
                            <h3 style="text-align:center; margin-bottom: 0px; font-size: <%=rsDet.getString("font_size")%>px;"><%=rsDet.getString("title")%></h3><br>
                            <p style="text-align:center; font-size: <%=rsDet.getString("font_size")%>px;"><%=rsDet.getString("details")%></p><br>
                            <%} else {%>
                            <h3 style="text-align:center; margin-bottom: 0px; "><%=rsDet.getString("title")%></h3><br>
                            <p style="text-align:center; "><%=rsDet.getString("details")%></p><br>
                            <%}%>
                            
                            <%}%>
                            
                        </div>
                                <%
                                    Statement st1 = db.connection.createStatement();
                                    String q1 = "select * from blog where id>0 order by id desc";
                                    ResultSet rs1 = st1.executeQuery(q1);
                                    while (rs1.next()) {
                                %>

                                <div class="row">
                                    <div class="col-sm-3" style="padding-bottom: 15px;">
          <div class="well">
           <img src="img/blog/<%=rs1.getString("id")%>.jpg" class="img-circle" alt="Avatar">
          </div>
        </div>
        <div class="col-sm-9">
          <div class="well">
              <h3 style="font-weight: bold; margin: 0px;  text-align:left; font-size: 27px;"><%=rs1.getString("title")%></h3>
            <p style="margin:0px;"><%=rs1.getString("details").substring(0, Math.min(300, rs1.getString("details").length() - 1))%></p>
            <%if (rs1.getString("details").length() > 300) {%>
            <a id="more" class="btn btn-link" style="float:right;" href="blogDetails.jsp?id=<%=rs1.getString("id")%>">READ MORE >></a>
            <%}%>
            <%if(session.getAttribute("email")!=null && session.getAttribute("email").equals("admin")) {%>
            <p><a href="addBlog.jsp?updateId=<%=rs1.getString("id")%>">Update Blog Post</a></p>
            <p><a href="addBlog.jsp?deleteId=<%=rs1.getString("id")%>">Delete Blog Post</a></p>
                            <%}%>
          </div>
        </div>
      </div>
          <br>                  
                                
          
          <%--<div class="row" style="padding: 10px;">
                                <div class="row" style="padding:5px;">

                                    <img src="img/blog/<%=rs1.getString("id")%>.jpg" style="width: 100%; height: auto;"/> 

                                </div>
                                <div class="row" style="padding-bottom:5px;">
                                    <h3 style="font-weight: bold; margin: 0px;"><%=rs1.getString("title")%></h3>
                                    <span class="teaser"><%=rs1.getString("details").substring(0, Math.min(300, rs1.getString("details").length() - 1))%></span>

                                    <span class="complete" id="complete" style="display: none;"><%=rs1.getString("details").substring(Math.min(300, rs1.getString("details").length() - 1), rs1.getString("details").length() - 1)%></span>

                                    <%if (rs1.getString("details").length() > 300) {%>
                                    <span class="more" id="more" onclick="location.href='blogDetails.jsp?id=<%=rs1.getString("id")%>'">cont. reading</span>
                                    <%}%>
                                </div>
                                <%if(session.getAttribute("email")!=null && session.getAttribute("email").equals("admin")) {%>
                                <div class="row" style="padding-bottom:5px;"><a href="addBlog.jsp?updateId=<%=rs1.getString("id")%>">Update Blog Post</a></div>
                                <div class="row" style="padding-bottom:5px;"><a href="addBlog.jsp?deleteId=<%=rs1.getString("id")%>">Delete Blog Post</a></div>
                            <%}%>
                            </div>
                            --%>
                            
                            
                            
                                <%}%>
                    </div>

                </div>
            </div>
        </section>
        
        <br><br><br><br>
        <%@ include file="footer.jsp" %>
    </body>
</html>
