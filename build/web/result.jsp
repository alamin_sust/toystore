<!DOCTYPE html>
<html lang="zxx">
    <head>
        <title>childhoodcomplete</title>
        <meta charset="UTF-8">
        <meta name="description" content=" Divisima | eCommerce Template">
        <meta name="keywords" content="divisima, eCommerce, creative, html">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Favicon -->
        <link href="img/logo.png" rel="shortcut icon"/>

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300,300i,400,400i,700,700i" rel="stylesheet">


        <!-- Stylesheets -->
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <link rel="stylesheet" href="css/font-awesome.min.css"/>
        <link rel="stylesheet" href="css/flaticon.css"/>
        <link rel="stylesheet" href="css/slicknav.min.css"/>
        <link rel="stylesheet" href="css/jquery-ui.min.css"/>
        <link rel="stylesheet" href="css/owl.carousel.min.css"/>
        <link rel="stylesheet" href="css/animate.css"/>
        <link rel="stylesheet" href="css/style.css"/>

        <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>

        <!--[if lt IE 9]>
                  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>
    <body>
        <%@ include file="header.jsp" %>

        <%
        
        Statement stB = db.connection.createStatement();
            String qB = "select * from banner where id=1";
            ResultSet bannerRs = stB.executeQuery(qB);
            bannerRs.next();
            
        %>
        
        <!-- Hero section -->
        <div class="container">
            <img src="img/banner.jpg" onclick="window.open('<%=bannerRs.getString("url")%>')" style="cursor: pointer;width: 100%; height: auto;"/>
        </div>
        
        <%
            
            String playCategory = request.getParameter("playCategory") == null ? "" : request.getParameter("playCategory");
            String color = request.getParameter("color") == null ? "" : request.getParameter("color");
            String subCategoryId = request.getParameter("subCategoryId") == null ? "" : request.getParameter("subCategoryId");
            String label = request.getParameter("label") == null ? "" : request.getParameter("label");
            String whereClause = "";
            if (!subCategoryId.equals("")) {
                if(!request.getParameter("color").equals("ALL COLORS")) {
                    whereClause += " and color='" + request.getParameter("color") + "'";
                }
                whereClause += " and play_category='" + request.getParameter("playCategory") + "'";
                whereClause += " and sub_category_id=" + request.getParameter("subCategoryId");


           %>

        <!-- Page info -->
        <div class="page-top-info">
            <div class="container">
                <div class="site-pagination">
                    <a href=""><%=label%></a>
                </div>
            </div>
        </div>
        <!-- Page info end -->

        <%} else if (request.getParameter("searchName") != null) {
                String[] arr = request.getParameter("searchName").split(" ");
                for(int i=0;i<arr.length;i++) {
                        whereClause += " and (tags like '%" + arr[i] + "%' or color like '%"+arr[i]+"%')";
                }
            
        %>
        <!-- Page info -->
        <div class="page-top-info">
            <div class="container">
                <h4>Search</h4>
                <div class="site-pagination">
                    <a href="#">Search by -> <%=request.getParameter("searchName")%></a>
                </div>
            </div>
        </div>
        <!-- Page info end -->
        <%}
        %>

        

        <!-- Category section -->
        <section class="category-section spad" style="padding-top: 10px !important;">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12  order-1 order-lg-2 mb-5 mb-lg-0">
                        <div class="row">
                            <%
                                Statement st = db.connection.createStatement();
                                String q = "select * from product where id>0" + whereClause;
                                System.out.println("---" + q);
                                ResultSet rs = st.executeQuery(q);
                                while (rs.next()) {
                            %>
                            <div class="col-lg-2 col-sm-2">
                                <div class="product-item">
                                    <div class="pi-pic">
                                        <%--<div class="tag-sale">ON SALE</div>--%>
                                        <img src="img/products/<%=rs.getString("id")%>.jpg" alt="" height="150px;" onclick="location.href='product.jsp?productId=<%=rs.getString("id")%>'" style="cursor: pointer;">
                                        <div class="pi-links">
                                            <%--<a href="product.jsp?productId=<%=rs.getString("id")%>" class="add-card"><i class="flaticon-menu"></i><span>Details</span></a>--%>
                                                    <%if (session.getAttribute("email") != null /*&& session.getAttribute("email").equals("admin")*/) {%>
                                            <a href="addProduct.jsp?type=Update&productId=<%=rs.getString("id")%>" class="add-card"><i class="flaticon-edit"></i><span>Update</span></a>
                                            <a href="addProduct.jsp?type=Delete&productId=<%=rs.getString("id")%>" class="add-card"><i class="flaticon-remove"></i><span>Delete</span></a>
                                                    <%}%>
                                        </div>
                                                    <%if(session.getAttribute("email")!=null /*&& session.getAttribute("email").equals("admin")*/
                                                            &&request.getParameter("allProducts")!=null){%>
                                        <div class="pi-text">
                                            <p>id: <%=rs.getString("id")%></p>
                                        </div>
                                        <%}%>
                                    </div>
                                </div>
                            </div>
                            <%}%>
                            <%--<div class="text-center w-100 pt-3">
                                    <button class="site-btn sb-line sb-dark">LOAD MORE</button>
                            </div>--%>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Category section end -->

        <%@ include file="footer.jsp" %>
    </body>
</html>
