<%-- 
    Document   : product
    Created on : Mar 1, 2019, 6:53:40 PM
    Author     : md_al
--%>

<%@page import="com.toystore.connection.Util"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<!DOCTYPE html>
<html lang="zxx">
    <head>
        <title>childhoodcomplete</title>
        <meta charset="UTF-8">
        <meta name="description" content=" Divisima | eCommerce Template">
        <meta name="keywords" content="divisima, eCommerce, creative, html">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Favicon -->
        <link href="img/logo.png" rel="shortcut icon"/>

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300,300i,400,400i,700,700i" rel="stylesheet">


        <!-- Stylesheets -->
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <link rel="stylesheet" href="css/font-awesome.min.css"/>
        <link rel="stylesheet" href="css/flaticon.css"/>
        <link rel="stylesheet" href="css/slicknav.min.css"/>
        <link rel="stylesheet" href="css/jquery-ui.min.css"/>
        <link rel="stylesheet" href="css/owl.carousel.min.css"/>
        <link rel="stylesheet" href="css/animate.css"/>
        <link rel="stylesheet" href="css/style.css"/>

        <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>

        <!--[if lt IE 9]>
                  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>
    <body>
        <%@ include file="header.jsp" %>

        <%
            if (request.getParameter("relatedProduct") != null) {
                String[] arr = request.getParameter("relatedProduct").split(" ");
                Statement st = db.connection.createStatement();
                String q = "delete from related_product where product_id=" + request.getParameter("productId");
                st.executeUpdate(q);
                for (int i = 0; i < arr.length; i++) {

                    Statement st2 = db.connection.createStatement();
                    String q2 = "insert into related_product(id,product_id) values(" + arr[i] + "," + request.getParameter("productId") + ")";
                    st2.executeUpdate(q2);
                    session.setAttribute("sM", "Successfully Submitted!");
                }
            }
        %>

        

        <%if (session.getAttribute("sM") != null) {%>
        <div class="alert alert-success text-center">
            <%=session.getAttribute("sM")%>
        </div>
        <%session.setAttribute("sM", null);
            }%>
        <%if (session.getAttribute("eM") != null) {%>
        <div class="alert alert-danger text-center">
            <%=session.getAttribute("eM")%>
        </div>
        <%session.setAttribute("eM", null);
            }%>
            


        <!-- product section -->
        <section class="product-section" style="padding-top:5px; padding-bottom: 25px;">
            <div class="container">
                <%--<div class="back-link">
                        <a href="./category.html"> &lt;&lt; Back to Category</a>
                </div>--%>
                <div class="row">
                    <div class="col-lg-4" style="padding-top:25px;">
                        <div class="product-pic-zoom"  style="height: auto;">
                            <img class="product-big-img" src="img/products/<%=request.getParameter("productId")%>.jpg" alt="">
                        </div>

                        <div class="product-thumbs" tabindex="1" style="overflow: hidden; outline: none;">
                            <div class="product-thumbs-track">
                                <%
                                Statement st3 = db.connection.createStatement();
                        String q3 = "select * from product where id="+request.getParameter("productId");
                        ResultSet rs3 = st3.executeQuery(q3);
                        rs3.next();
                                %>
                                <div style="width: 75px;height: 75px;" class="pt active" data-imgbigurl="img/products/<%=request.getParameter("productId")%>.jpg"><img src="img/products/<%=request.getParameter("productId")%>.jpg" alt=""></div>
                                <%for(int i=1;i<=rs3.getInt("additional_images");i++){%>
                                <div style="width: 75px;height: 75px;" class="pt" data-imgbigurl="img/products/<%=request.getParameter("productId")%>_<%=i%>.jpg"><img src="img/products/<%=request.getParameter("productId")%>_<%=i%>.jpg" alt=""></div>
                                <%}%>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 product-details">
                        <%
                            Statement st = db.connection.createStatement();
                            String q = "select * from product where id=" + request.getParameter("productId");
                            ResultSet rs = st.executeQuery(q);
                            rs.next();

                            Statement st2 = db.connection.createStatement();
                            String q2 = "select * from related_product where product_id=" + request.getParameter("productId");
                            ResultSet rs2 = st2.executeQuery(q2);
                            String relatedProduct = "";
                            while (rs2.next()) {
                                relatedProduct += rs2.getString("id") + " ";
                            }
                            rs2.beforeFirst();
                            relatedProduct = relatedProduct.trim();
                            


                String name = rs.getString("name");
                String details = rs.getString("details");
                String tags = rs.getString("tags");
                String codes = rs.getString("codes");
                String playCategory = rs.getString("play_category");
                String color = rs.getString("color");
                String subCategoryId = rs.getString("sub_category_id");

                String toyLines[] = Util.getSplittedTexts(rs.getString("toy_lines"));
                String toyLine1 = toyLines[0];
                String toyLine2 = toyLines[1];
                String toyLine3 = toyLines[2];
                String toyLine4 = toyLines[3];
                String toyLine5 = toyLines[4];
                String toyLine6 = toyLines[5];
                String toyLine7 = toyLines[6];
                String toyLine8 = toyLines[7];
                String toyLine9 = toyLines[8];
                String toyLine10 = toyLines[9];
                
                String partNames[] = Util.getSplittedTexts(rs.getString("part_names"));
                String partName1 = partNames[0];
                String partName2 = partNames[1];
                String partName3 = partNames[2];
                String partName4 = partNames[3];
                String partName5 = partNames[4];
                String partName6 = partNames[5];
                String partName7 = partNames[6];
                String partName8 = partNames[7];
                String partName9 = partNames[8];
                String partName10 = partNames[9];
                
                String brands[] = Util.getSplittedTexts(rs.getString("brands"));
                String brand1 = brands[0];
                String brand2 = brands[1];
                String brand3 = brands[2];
                String brand4 = brands[3];
                String brand5 = brands[4];
                String brand6 = brands[5];
                String brand7 = brands[6];
                String brand8 = brands[7];
                String brand9 = brands[8];
                String brand10 = brands[9];

                String associatedToys[] = Util.getSplittedTexts(rs.getString("associated_toys"));
                String associatedToy1 = associatedToys[0];
                String associatedToy2 = associatedToys[1];
                String associatedToy3 = associatedToys[2];
                String associatedToy4 = associatedToys[3];
                String associatedToy5 = associatedToys[4];
                String associatedToy6 = associatedToys[5];
                String associatedToy7 = associatedToys[6];
                String associatedToy8 = associatedToys[7];
                String associatedToy9 = associatedToys[8];
                String associatedToy10 = associatedToys[9];

                String years[] = Util.getSplittedTexts(rs.getString("years"));
                String year1 = years[0];
                String year2 = years[1];
                String year3 = years[2];
                String year4 = years[3];
                String year5 = years[4];
                String year6 = years[5];
                String year7 = years[6];
                String year8 = years[7];
                String year9 = years[8];
                String year10 = years[9];

                        
                        %>
                        <div id="accordion" style="padding: 25px;">
                            <div class="panel-body">
                                <%
                                for(int i=0;i<10;i++) {
                        if(partNames[i]==null||partNames[i].trim().equals("")) {
                            continue;
                        }    
                        
                                %>
                                <p><%=partNames[i]+" - "+toyLines[i]+" : "+associatedToys[i] + " ("+brands[i]+" - "+years[i]+")"%></p>
                            
                        <%}%>
                            </div>
                            <div class="panel-body">
                                <label>Additional Details:</label>
                                <p><%=details%></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- product section end -->


        <%if (session.getAttribute("email") != null) {%>
        <!-- Contact section -->
        <section class="contact-section">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 contact-info">
                        <form class="contact-form" action="product.jsp" method="get">
                            <textarea name="relatedProduct" placeholder="Related Product Id"><%=relatedProduct%></textarea>

                            <br>
                            <input type="hidden" name="productId" value="<%=request.getParameter("productId")%>">
                            <button class="site-btn" type="submit">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
            <%--<div class="map"><iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d14376.077865872314!2d-73.879277264103!3d40.757667781624285!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sbd!4v1546528920522" style="border:0" allowfullscreen></iframe></div>--%>
        </section>
        <!-- Contact section end -->
        <%}%>


        <!-- Category section -->
        <section class="category-section spad" style="padding-top: 0px;">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12  order-1 order-lg-2 mb-5 mb-lg-0">
                        <div class="row">
                            <h4 style="font-size: 16px;">RELATED ACCESSORIES</h4>
                        </div>
                        <div class="row">


                            <%while (rs2.next()) {%>
                            <div class="col-lg-2 col-sm-2">
                                <div class="product-item">
                                    <div class="pi-pic">
                                        <img src="img/products/<%=rs2.getString("id")%>.jpg" alt="" onclick="location.href='product.jsp?productId=<%=rs2.getString("id")%>'" style="cursor: pointer;" style="height: 150px;">
                                    </div>
                                </div>
                            </div>
                            <%}%>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Category section end -->

        <%@ include file="footer.jsp" %>
    </body>
</html>
