<%-- 
    Document   : footer
    Created on : Feb 26, 2019, 10:10:36 PM
    Author     : md_al
--%>

	<!-- Footer section -->
	<section class="footer-section">
		<div class="social-links-warp">
			<div class="container" style="text-align:center;">
				<div class="social-links">
					<a href="" class="instagram"><i class="fab fa-instagram"></i><span>instagram</span></a>
					<a href="" class="facebook"><i class="fab fa-facebook"></i><span>facebook</span></a>
					<a href="" class="twitter"><i class="fab fa-twitter"></i><span>twitter</span></a>
					<a href="" class="youtube"><i class="fab fa-ebay"></i><span>ebay</span></a>
                                        <a href="mailto:info@childhoodcomplete.com" class="pinterest"><i class="fab fa-telegram"></i><span>Contact Us</span></a>
				</div>

<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --> 
<p class="text-white text-center mt-5">Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a></p>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->

			</div>
		</div>
	</section>
	<!-- Footer section end -->



	<!--====== Javascripts & Jquery ======-->
	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.slicknav.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/jquery.nicescroll.min.js"></script>
	<script src="js/jquery.zoom.min.js"></script>
	<script src="js/jquery-ui.min.js"></script>
	<script src="js/main.js"></script>
<%
/*}catch(Exception e) {
System.out.println(e);
} finally {
db.close();
}
*/
%>
