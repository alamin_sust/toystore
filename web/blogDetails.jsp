<%-- 
    Document   : changeDetails
    Created on : Apr 12, 2019, 2:01:29 AM
    Author     : md_al
--%>

<%@page import="java.io.FileOutputStream"%>
<%@page import="java.io.File"%>
<%@page import="java.io.DataInputStream"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<!DOCTYPE html>
<html lang="zxx">
    <head>
        <title>childhoodcomplete</title>
        <meta charset="UTF-8">
        <meta name="description" content=" Divisima | eCommerce Template">
        <meta name="keywords" content="divisima, eCommerce, creative, html">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Favicon -->
        <link href="img/logo.png" rel="shortcut icon"/>

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300,300i,400,400i,700,700i" rel="stylesheet">


        <!-- Stylesheets -->
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <link rel="stylesheet" href="css/font-awesome.min.css"/>
        <link rel="stylesheet" href="css/flaticon.css"/>
        <link rel="stylesheet" href="css/slicknav.min.css"/>
        <link rel="stylesheet" href="css/jquery-ui.min.css"/>
        <link rel="stylesheet" href="css/owl.carousel.min.css"/>
        <link rel="stylesheet" href="css/animate.css"/>
        <link rel="stylesheet" href="css/style.css"/>

        <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>

        <!--[if lt IE 9]>
                  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>
    <body>
        <%@ include file="header.jsp" %>
        <%
            
            Statement st1 = db.connection.createStatement();
            String q1 = "select * from blog where id="+request.getParameter("id");
            ResultSet rs1 = st1.executeQuery(q1);
            rs1.next();
        %>


        

        <%if (session.getAttribute("sM") != null) {%>
        <div class="alert alert-success text-center">
            <%=session.getAttribute("sM")%>
        </div>
        <%session.setAttribute("sM", null);
            }%>
        <%if (session.getAttribute("eM") != null) {%>
        <div class="alert alert-danger text-center">
            <%=session.getAttribute("eM")%>
        </div>
        <%session.setAttribute("eM", null);
            }%>

            
        <!-- Contact section -->
        <section class="contact-section">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 contact-info">
                        <div class="row" style="padding:5px;">

                                    <img src="img/blog/<%=rs1.getString("id")%>.jpg" style="width: 600px; height: 600px;"/> 

                        </div>
                                    <br>
                        <div class="row" style="padding-bottom:5px;">
                            <div class="well">
                                    <h3 style="font-weight: bold; margin: 0px; margin-bottom: 5px; text-align: center;"><%=rs1.getString("title")%></h3>
                                    <span class="teaser" style="font-size: large;"><%=rs1.getString("details").substring(0, Math.min(300, rs1.getString("details").length() - 1))%></span>

                                    <span class="complete" id="complete" style="display: none;"><%=rs1.getString("details")%></span>

                            </div>
                                </div>
                                <%if(session.getAttribute("email")!=null && session.getAttribute("email").equals("admin")) {%>
                                <div class="row" style="padding-bottom:20px;"><a href="addBlog.jsp?updateId=<%=rs1.getString("id")%>">Update Blog Post</a></div>
                            <%}%>
                    </div>
                </div>
            </div>
            <%--<div class="map"><iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d14376.077865872314!2d-73.879277264103!3d40.757667781624285!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sbd!4v1546528920522" style="border:0" allowfullscreen></iframe></div>--%>
        </section>
        <!-- Contact section end -->



        <br><br><br><br><br><br><br><br><br><br><br>


        <%@ include file="footer.jsp" %>


    </body>
</html>


