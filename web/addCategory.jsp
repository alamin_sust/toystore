<%@page import="java.io.FileOutputStream"%>
<%@page import="java.io.File"%>
<%@page import="java.io.DataInputStream"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<!DOCTYPE html>
<html lang="zxx">
<head>
	<title>childhoodcomplete</title>
	<meta charset="UTF-8">
	<meta name="description" content=" Divisima | eCommerce Template">
	<meta name="keywords" content="divisima, eCommerce, creative, html">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Favicon -->
	<link href="img/logo.png" rel="shortcut icon"/>

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300,300i,400,400i,700,700i" rel="stylesheet">


	<!-- Stylesheets -->
	<link rel="stylesheet" href="css/bootstrap.min.css"/>
	<link rel="stylesheet" href="css/font-awesome.min.css"/>
	<link rel="stylesheet" href="css/flaticon.css"/>
	<link rel="stylesheet" href="css/slicknav.min.css"/>
	<link rel="stylesheet" href="css/jquery-ui.min.css"/>
	<link rel="stylesheet" href="css/owl.carousel.min.css"/>
	<link rel="stylesheet" href="css/animate.css"/>
	<link rel="stylesheet" href="css/style.css"/>

        <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>

	<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>
<body>
    <%@ include file="header.jsp" %>
    <%
        if (session.getAttribute("email") == null /*|| !session.getAttribute("email").equals("admin")*/) {
                response.sendRedirect("home.jsp");
            }
        
        
        String name = request.getParameter("name")==null?"":request.getParameter("name");
        String categoryId = request.getParameter("categoryId")==null?"":request.getParameter("categoryId");
        
        if(!name.equals("")) {
        
            Statement st = db.connection.createStatement();
        String q = "select * from category where id="+categoryId;
        ResultSet rs = st.executeQuery(q);
        rs.next();
            
            Statement st1 = db.connection.createStatement();
        String q1 = "select max(id)+1 as mx from sub_category";
        ResultSet rs1 = st1.executeQuery(q1);
        rs1.next();
        
        Statement st2 = db.connection.createStatement();
        String q2 = "insert into sub_category (id,name,category_id,category_name) values("
                +rs1.getString("mx")+",'"+name+"',"+categoryId+",'"+rs.getString("name")+"')";
        st2.executeUpdate(q2);
        session.setAttribute("sM", "Successfully Inserted!");
        
        if(categoryId.equals( "2")) {
            Statement st11 = db.connection.createStatement();
        String q11 = "select max(id)+1 as mx from category";
        ResultSet rs11 = st11.executeQuery(q11);
        rs11.next();
            Statement st22 = db.connection.createStatement();
            String q22 = "insert into category (id,name) values("
                +rs11.getString("mx")+",'"+name+"')";
                st22.executeUpdate(q22);
        }
        
        }
        
        
        
        
    %>
    

	<!-- Page info -->
	<div class="page-top-info">
		<div class="container">
                            
			<div class="site-pagination">
				<a href="#">Add New Category</a>
			</div>
		</div>
	</div>
	<!-- Page info end -->
        
        <%if(session.getAttribute("sM")!=null){%>
            <div class="alert alert-success text-center">
                <%=session.getAttribute("sM")%>
            </div>
        <%session.setAttribute("sM", null);}%>
        <%if(session.getAttribute("eM")!=null){%>
            <div class="alert alert-danger text-center">
                <%=session.getAttribute("eM")%>
            </div>
        <%session.setAttribute("eM", null);}%>

        <!-- Contact section -->
	<section class="contact-section">
		<div class="container">
                    <h2>Add Child Sub-Categories</h2>
			<div class="row">
				<div class="col-lg-6 contact-info">
                                    <form class="contact-form" action="addCategory.jsp" method="post">
                                        <input type="text" name="name" value="<%=name%>" placeholder="name" required="">
                                        
                                        
                                        <select name="categoryId" class="form-control" required="true">
                                            <option value="">--Select Sub Category--</option>
                                                <%
                                                Statement st2 = db.connection.createStatement();
                                                String q2 = "select * from category where id>2";
                                                ResultSet rs2 = st2.executeQuery(q2);
                                                while(rs2.next()) {
                                                
                                                %>
                                                <option value="<%=rs2.getString("id")%>" <%if(rs2.getString("id").equals(categoryId)){%>selected<%}%>><%=rs2.getString("name")%></option>
                                                <%}%>
                                            </select>
                                            <br>
                                            <button class="site-btn" type="submit">Insert</button>
					</form>
				</div>
			</div>
		</div>
		<%--<div class="map"><iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d14376.077865872314!2d-73.879277264103!3d40.757667781624285!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sbd!4v1546528920522" style="border:0" allowfullscreen></iframe></div>--%>
	</section>
	<!-- Contact section end -->
        
        <!-- Contact section -->
	<section class="contact-section">
		<div class="container">
                    <h2>Add Parent Categories</h2>
			<div class="row">
				<div class="col-lg-6 contact-info">
                                    <form class="contact-form" action="addCategory.jsp" method="post">
                                        <input type="text" name="name" value="<%=name%>" placeholder="name" required="">
                                        <select name="categoryId" class="form-control" required="true">
                                            <option value="">--Select Category--</option>
                                                <option value="1">COLOR</option>
                                                <option value="2">PLAY CATEGORY</option>
                                            </select>
                                            <br>
                                            <button class="site-btn" type="submit">Insert</button>
					</form>
				</div>
			</div>
		</div>
		<%--<div class="map"><iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d14376.077865872314!2d-73.879277264103!3d40.757667781624285!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sbd!4v1546528920522" style="border:0" allowfullscreen></iframe></div>--%>
	</section>
	<!-- Contact section end -->

        <br><br><br><br><br><br><br><br><br><br><br>


        <%@ include file="footer.jsp" %>
	</body>
</html>
