<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<!DOCTYPE html>
<html lang="zxx">
<head>
	<title>childhoodcomplete</title>
	<meta charset="UTF-8">
	<meta name="description" content=" Divisima | eCommerce Template">
	<meta name="keywords" content="divisima, eCommerce, creative, html">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Favicon -->
	<link href="img/logo.png" rel="shortcut icon"/>

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300,300i,400,400i,700,700i" rel="stylesheet">


	<!-- Stylesheets -->
	<link rel="stylesheet" href="css/bootstrap.min.css"/>
	<link rel="stylesheet" href="css/font-awesome.min.css"/>
	<link rel="stylesheet" href="css/flaticon.css"/>
	<link rel="stylesheet" href="css/slicknav.min.css"/>
	<link rel="stylesheet" href="css/jquery-ui.min.css"/>
	<link rel="stylesheet" href="css/owl.carousel.min.css"/>
	<link rel="stylesheet" href="css/animate.css"/>
	<link rel="stylesheet" href="css/style.css"/>

        <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>

	<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>
<body>
    <%@ include file="header.jsp" %>
    <%
        if(request.getParameter("Logout")!=null) {
        session.setAttribute("id", null);
        session.setAttribute("email", null);
        response.sendRedirect("home.jsp");
        }
        
        String type = "Login";
        if(request.getParameter("type")!=null&&request.getParameter("type").equals("Register")){
            type = "Register";
        }
        
        if(type.equals("Register") &&( session.getAttribute("email")==null || !session.getAttribute("email").equals("admin"))) {
            response.sendRedirect("home.jsp");
        }
        
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        if(email !=null && type.equals("Register")) {
        Statement st = db.connection.createStatement();
        String q = "select * from user where email='"+email+"'";
        ResultSet rs = st.executeQuery(q);
        
        if(rs.next()) {
            session.setAttribute("eM", "User Already Exists!");
        } else {
            Statement st2 = db.connection.createStatement();
            String q2 = "select max(id)+1 as mx from user";
            ResultSet rs2 = st2.executeQuery(q2);
            rs2.next();
            
            Statement st3 = db.connection.createStatement(); 
            String q3 = "insert into user(id,email,password) values("+rs2.getString("mx")+",'"+email+"','"+password+"');";
            st3.executeUpdate(q3);
            session.setAttribute("id", rs2.getString("mx"));
            session.setAttribute("email", email);
            session.setAttribute("sM2", "Successfully Registered!");
            response.sendRedirect("home.jsp");
        }
        } else if(email !=null) {
            Statement st = db.connection.createStatement();
            String q = "select * from user where email='"+email+"'";
            ResultSet rs = st.executeQuery(q);
            
            if(rs.next()) {
                if(rs.getString("password").equals(password)) {
                    session.setAttribute("id", rs.getString("id"));
                    session.setAttribute("email", email);
                    session.setAttribute("sM2", "Successfully Logged In!");
                    response.sendRedirect("home.jsp");
                } else {
                    session.setAttribute("eM", "Password Missmatch!");
                }
            } else {
                session.setAttribute("eM", "User Doesn't Exists!");
            }
        }
        
        
        
        
    %>
    

	<!-- Page info -->
	<div class="page-top-info">
		<div class="container">
                    <h4>
                        <%=type%>
                    </h4>
                            
			<div class="site-pagination">
				<a href="home.jsp">Home</a> /
				<a href="#"><%=type%></a>
			</div>
		</div>
	</div>
	<!-- Page info end -->
        
        <%if(session.getAttribute("sM")!=null){%>
            <div class="alert alert-success text-center">
                <%=session.getAttribute("sM")%>
            </div>
        <%session.setAttribute("sM", null);}%>
        <%if(session.getAttribute("eM")!=null){%>
            <div class="alert alert-danger text-center">
                <%=session.getAttribute("eM")%>
            </div>
        <%session.setAttribute("eM", null);}%>

        <!-- Contact section -->
	<section class="contact-section">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 contact-info">
                                    <form class="contact-form" action="login.jsp" method="post">
                                            <input type="text" name="email" placeholder="email" required="">
                                            <input type="password" name="password" placeholder="password" required="">
                                            <input type="hidden" name="type" value="<%=type%>">
                                            <button class="site-btn" type="submit"><%=type%></button>
					</form>
				</div>
			</div>
		</div>
		<%--<div class="map"><iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d14376.077865872314!2d-73.879277264103!3d40.757667781624285!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sbd!4v1546528920522" style="border:0" allowfullscreen></iframe></div>--%>
	</section>
	<!-- Contact section end -->

        <br><br><br><br><br><br><br><br><br><br><br>


        <%@ include file="footer.jsp" %>
	</body>
</html>
