-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: toystore
-- ------------------------------------------------------
-- Server version	5.7.18-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `banner`
--

DROP TABLE IF EXISTS `banner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banner` (
  `url` varchar(1010) NOT NULL,
  `id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banner`
--

LOCK TABLES `banner` WRITE;
/*!40000 ALTER TABLE `banner` DISABLE KEYS */;
INSERT INTO `banner` VALUES ('facebook.com',1);
/*!40000 ALTER TABLE `banner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blog`
--

DROP TABLE IF EXISTS `blog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blog` (
  `id` int(11) NOT NULL,
  `title` varchar(1010) NOT NULL,
  `details` varchar(3010) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog`
--

LOCK TABLES `blog` WRITE;
/*!40000 ALTER TABLE `blog` DISABLE KEYS */;
INSERT INTO `blog` VALUES (0,'0','0'),(1,'title88','LLL aaa888rtyrety rtyrety ertyerty ertyertyertyertyertyertyetyertyertyetyerty etye y rtyer tye'),(2,'Added Utility Tracking','dfgrty ertyerty ertyert yertyerty ertyerty erty wrtyertyertyertyert'),(3,'Added Utility Tracking','ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD '),(4,'Added Utility Tracking','ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD '),(5,'Added Utility Tracking','ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD '),(6,'title11','qwerqw11 rtye ret ertyert yerty erty ert yerty erty erty erty ert yerty ety ertyertyerty erty'),(7,'rtewrt5599','hfgh hdfghf6699 rte ertwert wrew tewrt ert wertwertwe  ewrt wertwert wertw ert wertwer twer twert wert wert wert wert wert wret wrtwe rtwer');
/*!40000 ALTER TABLE `blog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(110) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'COLOR'),(2,'PLAY CATEGORY'),(3,'ACTION FIGURE'),(4,'VEHICLE'),(5,'PLAYSET'),(6,'ROLEPLAY2'),(7,'GAME / BOARDGAME'),(8,'FOOD PREMIUM');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `name` varchar(110) DEFAULT NULL,
  `details` varchar(1010) DEFAULT NULL,
  `sub_category_id` int(11) DEFAULT NULL,
  `tags` varchar(1010) DEFAULT NULL,
  `color` varchar(110) DEFAULT NULL,
  `play_category` varchar(110) DEFAULT NULL,
  `codes` varchar(1010) DEFAULT NULL,
  `toy_lines` varchar(4000) DEFAULT NULL,
  `associated_toys` varchar(4000) DEFAULT NULL,
  `years` varchar(4000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (0,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1,'','ff',15,'t T','BLACK / GRAY / SILVER','ACTION FIGURE','c C',NULL,NULL,NULL),(3,'','datails toy test',15,'man male USA','BLUE / PURPLE','ACTION FIGURE','C1 C2',NULL,NULL,NULL),(4,'','Det',15,'U V','BLACK / GRAY / SILVER','ACTION FIGURE','cc rr',NULL,NULL,NULL),(5,'','deta',15,'fsd sdfa','BLACK / GRAY / SILVER','ACTION FIGURE','sdf ',NULL,NULL,NULL),(6,'','dett',17,'tt','BLACK / GRAY / SILVER','ACTION FIGURE','',NULL,NULL,NULL),(7,'','rtwer',17,'tt t1','BLACK / GRAY / SILVER','ACTION FIGURE','',NULL,NULL,NULL),(8,'','det',15,'t1 t2','BLACK / GRAY / SILVER','ACTION FIGURE','',NULL,NULL,NULL),(9,'p n','',15,'','BLACK / GRAY / SILVER','ACTION FIGURE','','tl 1#tl 2#tl 3#tl 4','#########','#########'),(10,'pnnnn1','r',15,'t y','BLACK / GRAY / SILVER','ACTION FIGURE','','tl 1#tl 2#tl 3#tl 4','a#s#d#f#h#j#k#l#n#m','z#x#c#v#b#n#m#q#w#e'),(11,'aa','qwer wqer',16,'r rr','BLACK / GRAY / SILVER','ACTION FIGURE','',' q# w# e# r',' y# u# m# mj# r# g# h# s# g# e',' t# t# gs# a# er# r r# te# t# t#  t');
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `related_product`
--

DROP TABLE IF EXISTS `related_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `related_product` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `related_product`
--

LOCK TABLES `related_product` WRITE;
/*!40000 ALTER TABLE `related_product` DISABLE KEYS */;
INSERT INTO `related_product` VALUES (5,4),(1,5),(1,11),(2,11);
/*!40000 ALTER TABLE `related_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sub_category`
--

DROP TABLE IF EXISTS `sub_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sub_category` (
  `id` int(11) NOT NULL,
  `name` varchar(110) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `category_name` varchar(110) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sub_category`
--

LOCK TABLES `sub_category` WRITE;
/*!40000 ALTER TABLE `sub_category` DISABLE KEYS */;
INSERT INTO `sub_category` VALUES (1,'BLACK / GRAY / SILVER',1,'COLOR'),(2,'WHITE / OFF-WHITE',1,'COLOR'),(3,'RED / PINK / MAROON / ORANGE',1,'COLOR'),(4,'BROWN / TAN',1,'COLOR'),(5,'BLUE / PURPLE',1,'COLOR'),(6,'GREEN / YELLOW / GOLD2',1,'COLOR'),(7,'CLEAR / TRANSPARENT',1,'COLOR'),(8,'ALL COLORS',1,'COLOR'),(9,'ACTION FIGURE',2,'PLAY CATEGORY '),(10,'VEHICLE',2,'PLAY CATEGORY '),(11,'PLAYSET',2,'PLAY CATEGORY'),(12,'ROLEPLAY2',2,'PLAY CATEGORY'),(13,'GAME / BOARDGAME',2,'PLAY CATEGORY'),(14,'FOOD PREMIUM',2,'PLAY CATEGORY'),(15,'HANDHELD',3,'ACTION FIGURE'),(16,'BODY',3,'ACTION FIGURE'),(17,'CONNECTION DEVICE',3,'ACTION FIGURE'),(18,'INDEPENDANT TRANSPORTATION',3,'ACTION FIGURE'),(19,'DISPLAY / STAND',3,'ACTION FIGURE'),(20,'PACK-IN',3,'ACTION FIGURE'),(21,'BODY / CHASIS',4,'VEHICLE'),(22,'WEAPON ATTACHMENTS',4,'VEHICLE'),(23,'TOOL ATTACHMENTS',4,'VEHICLE'),(24,'PROJECTILES',4,'VEHICLE'),(25,'WHEELS / TRACKS / TREADS',4,'VEHICLE'),(26,'STRUCTURAL ATTACHMENTS',4,'VEHICLE'),(27,'OPERATIONAL ATTACHMENTS',4,'VEHICLE'),(28,'INDEPENDENT WEAPONS',4,'VEHICLE'),(29,'BATTERY COVERS',4,'VEHICLE'),(30,'DISPLAYS / SCENERY',4,'VEHICLE'),(31,'CANOPIES / WINDSHIELDS',4,'VEHICLE'),(32,'CONNECTION DEVICES',4,'VEHICLE'),(33,'SEAT BELTS',4,'VEHICLE'),(34,'PRIMARY STRUCTURES',5,'PLAYSET'),(35,'SECONDARY STRUCTURES',5,'PLAYSET'),(36,'WEAPON ATTACHMENTS',5,'PLAYSET'),(37,'TOOL ATTACHMENTS',5,'PLAYSET'),(38,'PROJECTILES',5,'PLAYSET'),(39,'BASES',5,'PLAYSET'),(40,'STRUCTURAL ATTACHMENTS',5,'PLAYSET'),(41,'OPERATIONAL ATTACHEMNTS',5,'PLAYSET'),(42,'INDEPENDENT WEAPONS',5,'PLAYSET'),(43,'BATTERY COVERS',5,'PLAYSET'),(44,'DISPLAY / SCENERY',5,'PLAYSET'),(45,'CANOPIES / WINDSHIELDS',5,'PLAYSET'),(46,'CONNECTION DEVICES',5,'PLAYSET'),(47,'BODYWEAR',6,'ROLEPLAY2'),(48,'WEAPON',6,'ROLEPLAY2'),(49,'PROJECTILE',6,'ROLEPLAY2'),(50,'FASHION2',6,'ROLEPLAY2'),(51,'PACK-IN',6,'ROLEPLAY2'),(52,'GAME PIECE',7,'GAME / BOARDGAME'),(53,'GAME BOARD',7,'GAME / BOARDGAME'),(54,'RANDOM CHANCE DEVICE',7,'GAME / BOARDGAME'),(55,'GAME CARD',7,'GAME / BOARDGAME'),(56,'GAMEPLAY ACCESSORY',7,'GAME / BOARDGAME'),(57,'VERTICAL STRUCTURE',7,'GAME / BOARDGAME'),(58,'BATTERY COVER',7,'GAME / BOARDGAME'),(59,'FIGURE',8,'FOOD PREMIUM'),(60,'VEHICLE',8,'FOOD PREMIUM'),(61,'ALL-IN-ONE RIDING TOY',8,'FOOD PREMIUM');
/*!40000 ALTER TABLE `sub_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` varchar(110) NOT NULL,
  `password` varchar(110) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (0,'0',NULL),(1,'admin','11'),(2,'admin2','11'),(3,'admin3','11');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `website_details`
--

DROP TABLE IF EXISTS `website_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `website_details` (
  `title` varchar(1010) NOT NULL,
  `details` varchar(3010) NOT NULL,
  `is_bold` int(11) NOT NULL,
  `font_size` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `website_details`
--

LOCK TABLES `website_details` WRITE;
/*!40000 ALTER TABLE `website_details` DISABLE KEYS */;
INSERT INTO `website_details` VALUES ('title','ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ABCD ',1,15,1);
/*!40000 ALTER TABLE `website_details` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-04-20  1:27:21
