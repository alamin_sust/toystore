alter table product add column part_names varchar(4000);
alter table product add column brands varchar(2000);
alter table product add column additional_images int default 0;